# -*- coding: utf-8 -*-
# @Time: 2023/2/1 11:49
# @Author: MinChess
# @File: doc.py
# @Software: PyCharm
#创建并写入word文档
import docx


#创建内存中的word文档对象
file=docx.Document()

file.add_heading(text="这是一级标题", level=1)
file.add_paragraph("发发发发发发付付付付付付付")


file.add_heading(text="这是二级标题", level=2)
file.add_paragraph("发发发发发发付付付付付付付付付")

#保存
file.save("./test.docx")
