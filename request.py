# -*- coding: utf-8 -*-
# @Time: 2023/2/1 10:29
# @Author: MinChess
# @File: request.py
# @Software: PyCharm
import requests
from lxml import etree

headers = {
    'Cookie': 'clickbids=116117; Hm_lvt_6dfe3c8f195b43b8e667a2a2e5936122=1675151965,1675218073; Hm_lpvt_6dfe3c8f195b43b8e667a2a2e5936122=1675218073',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
}



def catalog_get():
    cata_url = 'https://www.biquzw.la/116_116117/'
    response = requests.get(cata_url, headers=headers)
    response.encoding = 'utf-8'
    # print(response.text)
    html = etree.HTML(response.text, parser=etree.HTMLParser(encoding='utf-8'))
    # print(html)
    i = 1
    while True:
        href = html.xpath('//*[@id="list"]/dl/dd[' + str(i) + ']/a/@href')
        title = html.xpath('//*[@id="list"]/dl/dd[' + str(i) + ']/a/@title')
        if len(href):
            print(href, title)
            i = i + 1
            content_get(title[0],href[0])
        else:
            print("爬取完成")
            break


def content_get(title,url):
    file = open(r'./明克街/'+title+'.txt', mode='a', encoding='utf-8')
    file.write('## ' + title + '\n')
    content_url = 'https://www.biquzw.la/116_116117/'
    content_response = requests.get(content_url + url, headers=headers)
    content_response.encoding = 'utf-8'
    # print(content_response.text)
    html = etree.HTML(content_response.text,parser=etree.HTMLParser(encoding='utf-8'))
    content = html.xpath('//*[@id="content"]/text()')
    # a_res = etree.tostring(content[0], encoding='utf-8').strip().decode('utf-8')
    # print(a_res)
    # print(content)
    for i in range(len(content)):
        # print(content[i])
        file.write(content[i]+'\n')
    file.close()


if __name__ == '__main__':
    catalog_get()
